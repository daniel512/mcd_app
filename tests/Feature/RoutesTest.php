<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RoutesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/');
        $response->assertStatus(200); 
    }

    public function testLogin()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testCMS()
    {
        $response = $this->get('/cms');

        $response->assertStatus(200);
    }

    public function testCMSPosts()
    {
        $response = $this->get('/cms_posts');

        $response->assertStatus(200);
    }

    public function testPosts()
    {
        $response = $this->get('/posts');

        $response->assertStatus(200);
    }
}

