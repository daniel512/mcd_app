<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CMS</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <link rel="stylesheet" href="{{ url('/css/app.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>

      
    </style>
</head>
<body>
    <div class="cms">
        <div id="contextMenu" class="context-menu">
            <div id="delete" class="control">Usuń</div>
            <div id="moveUp" class="control">Przesuń w górę</div>
            <div id="moveDown" class="control">Przesuń w dół</div>
        </div>
        <div class="control-panel">
            <div class="inputs">
                <div class="inputs-container">
                    <input type="text" placeholder="tytuł" id="postTitle" />
                    <select>
                        <option> Mieszkania </option>
                    </select>
                </div> 
           
            </div>
            <div class="main-image">
                <div class="image-preview"></div>
                <div class="div-button"> Wybierz </div>
            </div>
            <div class="post-content">
                <div class="controls">
                    <div class="control" id="header1">
                        H1
                    </div>
                    <div class="control" id="header2">
                        H2
                    </div>
                    <div class="control" id="header3">
                        H3
                    </div>
                    <div class="control" id="header4">
                        H4
                    </div>
                    <div class="control" id="header5">
                        H5
                    </div>
                    <div class="control" id="paragraph">
                        P
                    </div>
              
                    <div class="control" id="image">
                        IMG
                    </div>
                    <div class="control" id="add">
                        Dodaj
                    </div>
                </div>
                <button id="btn1">klik</button>
                <button id="btn2">JSON</button>
                <button id="btn3">TO JSON</button>
            </div>
    
        </div>
        <div class="preview-container">
            <div class="preview" id="preview"></div>  
        </div>
    </div>

    <script sync src="{{ asset('js/app.js') }}"></script>
    <script>
        const d   = document;
        const cms = CMS;

        let preview     = d.getElementById('preview');
        let contextMenu = d.getElementById('contextMenu');
        cms.setPreviewElement(preview);
        cms.setContextMenu(contextMenu);
        
        d.getElementById('btn1').onclick = () => cms.addTable();
        d.getElementById('btn2').onclick = () => cms.test123();
        d.getElementById('btn3').onclick = () => cms.toJSON();

        let add = d.getElementById("add");

        let h1  = d.getElementById('header1');
        let h2  = d.getElementById('header2');
        let h3  = d.getElementById('header3');
        let h4  = d.getElementById('header4');
        let h5  = d.getElementById('header5');
        let p   = d.getElementById('paragraph');
        let img = d.getElementById('image');

        let ctxDelete = d.getElementById('delete');
        let moveUp    = d.getElementById('moveUp');
        let moveDown  = d.getElementById('moveDown');

        h1.onclick = () =>  cms.addTextElement('h1');
        h2.onclick = () =>  cms.addTextElement('h2');
        h3.onclick = () =>  cms.addTextElement('h3');
        h4.onclick = () =>  cms.addTextElement('h4');
        h5.onclick = () =>  cms.addTextElement('h5');
        p.onclick  = () =>  cms.addTextElement('p');

        ctxDelete.onclick = () => cms.deleteElement();
        moveUp.onclick    = () => cms.moveUp();
        moveDown.onclick  = () => cms.moveDown();

        add.onclick = () =>
        {
            let postTitle = d.getElementById('postTitle').value;
            let json = cms.toJSON();
            sendRequest(json, postTitle);
        }

        window.onclick = () => cms.hideContextMenu();
        



        let token = d.querySelector('meta[name="csrf-token"]').attributes[1].value;
 

        function sendRequest(json, postTitle) 
        {
            var request = $.ajax({
                url: 'http://todolist.stg/posts/create',
                type: 'POST',
                data: {
                    json:  json,      
                    postTitle: postTitle 
                }, 
                headers:{
                    'X-CSRF-TOKEN': token          
                }            
            });

            request.done(function(data) {
                preview.innerHTML = data;
            });

            request.fail(function(jqXHR, textStatus) {
                // your failure code here
            });
        }

    </script>
</body>
</html>