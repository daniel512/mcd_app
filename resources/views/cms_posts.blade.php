<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ url('/css/app.css') }}">
    <title>CMS POSTS</title>
</head>
<body>
    <div class="posts-list">
        @foreach ($posts as $post)
            <div class="post">
                <img src="{{$post->image}}" alt='GRAFIKA'/>
                <span> {{$post->title}} </span>
            </div>
        @endforeach
    </div>
</body>
</html>