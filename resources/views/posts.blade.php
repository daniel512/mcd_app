@extends('layouts.app')

@section('content')
  
  
  
    <section class="blog blog-m">
        <h2>Aktualności - M</h2>
        <div class="recent flex-center">
            @for ($i = 0; $i < 2; $i++)
            @if (count($posts) > $i)
                    <div class="post-card ">   
                        <div class="post-image"></div>        
                        <div class="description">
                            <div class="title">
                                {{ $posts[$i]->title }}
                            </div>
                            <div class="date">
                                {{ $posts[$i]->created_at }}
                            </div>
                        </div>
                        <a href="post/{{$posts[$i]->slug}}">
                            <div class="link-shutter"></div>
                        </a>
                        <div class="shutter"></div>
                    </div> 
                    @endif
            @endfor
        </div>
        
    </section>

    <section class="blog blog-s">
       
        <h2>Aktualności - S</h2>
    
        <div class="recent flex-center">
            @for ($i = 2; $i < 12; $i++)
            @if (count($posts) > $i)
            <div class="post-card ">
                <div class="post-image"></div>
                <div class="description">
                    <div class="title">
                        {{ $posts[$i]->title }}
                    </div>
                    <div class="date">
                        {{ $posts[$i]->created_at }}
                    </div>
                </div>
                <a href="post/{{$posts[$i]->slug}}">
                    <div class="link-shutter"></div>
                </a>
            </div>
            @endif
            @endfor
           
        </div>  
    </section>

    <section class="blog blog-m">
        <h2>Aktualności - M</h2>
        <div class="recent flex-center">
           
                
        
            @for ($i = 12; $i < 30; $i++)
                @if (count($posts) > $i)
                    <div class="post-card ">   
                        <div class="post-image"></div>        
                        <div class="description">
                            <div class="title">
                                {{ $posts[$i]->title }}
                            </div>
                            <div class="date">
                                {{ $posts[$i]->created_at }}
                            </div>
                        </div>
                        <a href="post/{{$posts[$i]->slug}}">
                            <div class="link-shutter"></div>
                        </a>
                        <div class="shutter"></div>
                    </div> 
                @endif
            @endfor
           
        </div>
        
    </section>

@endsection