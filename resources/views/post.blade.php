@extends('layouts.app')

<style>
    .content:
    {
        min-height: 0;
        height: 0;
    }
</style>
<div id="postContent">
    <h2>{{$post->title}}</h2>
    <p> {{$post->created_at}} | {{$category}}</p>
    <img src="{{$post->image}}" />
</div>

<script sync src="{{ asset('js/app.js') }}"></script>
<script>
    let post = document.getElementById('postContent');
    let cms = CMS;

    let json = {!! $post->body !!};

    cms.JSONToHTML(json, post);

</script>