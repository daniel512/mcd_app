<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Todo List</title>
    <link rel="stylesheet" href="{{ url('/css/app.css') }}">

</head>
<body>
    <x-navbar/>
    <div class="content">
        @yield('content')
    </div>
 

    <x-footer/>
    <script src="{{ url('/js/app.js') }}"></script>
</body>
</html>