 /**
  * Poniższa klasa zawiera metody pozwalające na tworzenie oraz edytowanie 
  * treści HTML z podglądem na żywo. Może być użyta np. do tworzenia treści
  * postów. Odpowiedni wygląd elementów podglądu treści powinien być zdefiniowany
  * w arkuszach stylu. Utworzona treść jest zapisywana do formatu JSON przy użyciu
  * metody toJSON() i może zostać przetworzona z powrotem do HTML przy
  * użyciu metody JSONToHTML().
  */
export class CMS 
{
    constructor() {}

    /**  
     *  metoda do ustawienia elementu służącego
     *  jako podląd treści. Do niego będą trafiać
     *  wszystkie nowo utworzone elementy
     */
    setPreviewElement(previewElement)
    {   
        this.preview = previewElement;
    }

    /**  
     *  do metody przekazujemy element, który będzie 
     *  naszym context menu pojawiającym się po naciśnięciu
     *  PPM na elemecie w poglądzie treści. 
     */
    setContextMenu(contextMenu)
    {
        this.contextMenu = contextMenu;
    }

    /**  
     *  metoda wywoływana przy zdarzeniu 'context menu',
     *  na 'selectedElement' będą oddziaływały metody
     *  wywyływane w contextMenu
     */
    setSelectedElement(selectedElement)
    {
        this.selectedElement = selectedElement;
    }

    /** ####   METODY DLA CONTEXT MENU   ####
     *  poniższe metody służą do usuwania, bądź 
     *  zmieniania elementów dodanych do podlądu treści. 
     *  Aby działały poprawnie wybrany element musi zostać 
     *  przypisany do 'selectedElement'. Ma to miejsce 
     *  w zdarzeniu 'oncontextmenu' dla każdego elementu.
     */
    deleteElement()
    {
        let tag = this.selectedElement.tagName;
        
        switch(tag)
        {
            case 'TABLE':
                this.selectedElement.remove();
            break;

            case 'TEXTAREA':
                this.selectedElement.remove();
                this.addTextareasListeners();
            break;

            default:
                console.log('delete');
        }
     
        this.hideContextMenu();
    }

    moveUp()
    {
        let index    = this.getIndexOfSelectedElement();
        let elements = this.preview;
        elements.insertBefore(this.selectedElement, elements.children[index - 1]);
        this.addTextareasListeners()
    }

    moveDown()
    {
        let index    = this.getIndexOfSelectedElement();
        let elements = this.preview;
        elements.insertBefore(this.selectedElement, elements.children[index + 2]);
        this.addTextareasListeners()
    }

    /**  
     *  metoda dodaje edytowalny element do
     *  podglądu treści. Atrybut 'type'  określa
     *  jaki znaczik chcemy utworzyć i jest używany
     *  w pliku css by zmienić wygląd w podlądzie treści
     *  oraz w metodzie textareaToJSON. Dla parametru 'type'
     *  podajemy nazwę znacznika tekstowego np. 'h1'
     */
    addTextElement(type)
    {
        let el = document.createElement('textarea');
        el.setAttribute('type', type);
        el.setAttribute('class', 'contentElement');
        this.preview.appendChild(el);
        this.addTextareasListeners();
    }

    textareaToJSON(textarea)
    {
        let content = textarea.value;
        let tag     = textarea.getAttribute('type');

        return {tag: tag, content: content}
    }

    /**  
     *  metoda musi być wywoływana przy dodawaniu,
     *  zmienianiu, bądź usuwaniu elementów w 
     *  podglądzie treści.
     */
    addTextareasListeners()
    {
        let textareas = document.getElementsByTagName('textarea');

        for(let i = 0; i < textareas.length; i++)
        {
            textareas[i].oncontextmenu = (e) =>
            {
                e.preventDefault();
                this.setSelectedElement(textareas[i])
                this.showContextMenu(e);
            }
        }
    }

    /**  
     *  metoda dodaje edytowalną tabelę o wymiarach
     *  podanych przez użytkownika do podlądu treści.
     */
    addTable() 
    {
        let table = document.createElement('table');
        table.setAttribute('class', 'contentElement');

        let numOfRows   = parseInt( window.prompt('ilość rzędów') );
        if ( !(numOfRows) == true )
        {
            alert('Nieprawidłowa wartość');
            return
        }
        let numOfColums = parseInt( window.prompt('ilość kolumn') );
        if ( !(numOfColums) == true )
        {
            alert('Nieprawidłowa wartość');
            return
        }

        for(let i = 0; i < numOfRows; i++)
        {
            let row = document.createElement('tr');
            for(let j = 0; j < numOfColums; j++)
            {
                let cell = document.createElement('td');
                row.appendChild(cell)
            }
            table.appendChild(row);
        }
        this.preview.appendChild(table);
        this.addTableCellsListeners();
    }

    /**  
     *  metoda przypisana dla każdej komórki tabeli
     */
    tableCellClickHandler(tableCell)
    {
        let text            = window.prompt('Wpisz zawartość komórki', tableCell.innerText);
        tableCell.innerText = text;
    }

    /**  
     *  metoda wywoływana w addTable, dodaje 
     *  eventListener dla każdej komórki tabeli
     */
    addTableCellsListeners()
    {
        let tableCells = document.getElementsByTagName('td');

        for(let i = 0; i < tableCells.length; i++)
        {
            tableCells[i].onclick = () => 
            {
                this.tableCellClickHandler(tableCells[i]);
            }
            tableCells[i].oncontextmenu = (e) =>
            {
                e.preventDefault();
                this.setSelectedElement(tableCells[i].parentElement.parentElement)
                this.showContextMenu(e);
            }
        }
    }

    /**
     *  Metoda przyjmuje tabelę w HTML
     *  i przetwarza ją na JSONa
     */
    tableToJSON(table)
    {
        let json = {tag: 'table', table: []};
      
        for(let i = 0; i < table.childNodes.length; i++)
        {
            let cells = [];
            let tr    = table.childNodes[i];

            for(let j = 0; j < tr.childNodes.length; j++)
            {
                let cell = tr.childNodes[j].innerText;
                cells.push(cell);
            }
            json.table.push(cells);
        }
       
        return json
    }

    /**
     *  Metoda przetwarza tabelę w formacie JSON na
     *  gotowy element DOM, który możemy dodać
     *  w dowolne miejsce
     */ 
    tableToHTML(tableJSON)
    {
        let table = document.createElement('table');
        for(let i = 0; i < tableJSON.length; i++)
        {
            let row = document.createElement('tr');
            for(let j = 0; j < tableJSON[i].length; j++)
            {
                let cell       = document.createElement('td');
                cell.innerText = tableJSON[i][j];
                row.appendChild(cell);
            }
            table.appendChild(row);
        }
        return table;
    }
    
    /**
     *  Metoda przyjmuje JSONa ze wszystimi elementami
     *  a następnie umieszcza je w miejscu wskazanym
     *  w parametrze 'domElement'
     */
    JSONToHTML(json, domElement)
    {
        for(let i = 0; i < json.elements.length; i++)
        {
            let tag     = json.elements[i].tag;
            let content = json.elements[i].content;
            let el      = document.createElement(tag);  
            
            switch(tag)
            {
                case 'img':
                    console.log('img');
                break;

                case 'table':
                    el = this.tableToHTML(json.elements[i].table);
                break;

                default:
                    el.innerText = content;
            }

            domElement.appendChild(el);
        }
    }
 
    /**
     *  Metoda przetwarza wszystkie elementy z podlądu
     *  treści do formatu JSON.
     */
    toJSON()
    {
        let elements = this.preview.getElementsByClassName('contentElement');
        let json     = {elements: []};
        
        for(let i = 0; i < elements.length; i++)
        {
            switch(elements[i].tagName)
            {
                case 'TABLE':
                    let table       = this.tableToJSON(elements[i]);
                    json.elements.push(table);
                break;

                case 'TEXTAREA':
                    let textElement = this.textareaToJSON(elements[i]);
                    json.elements.push(textElement);
                break;

                default:
                    console.log('nic');
            }
        }
        return json;
    }

    showContextMenu(e)
    {
        this.contextMenu.style.display = "block";
        let posX = e.clientX;
        let posY = e.clientY;
        this.contextMenu.style.top  = ` ${posY}px `;
        this.contextMenu.style.left = ` ${posX}px `;
    }

    hideContextMenu()
    {
        this.contextMenu.style.display = "none";
    }

    getIndexOfSelectedElement()
    {
        let el    = this.selectedElement;
        let index = [...el.parentElement.children].indexOf(el);
       
        return index;
    }

}
