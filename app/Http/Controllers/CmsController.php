<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class CmsController extends Controller
{
    public function new()
    {
        return view('cms');
    }

    public function posts()
    {
        $posts = Post::all();

        return view('cms_posts' ,['posts'=> $posts]);
    }

    public function edit($id)
    {
        $post = Post::find($id);

        return view('cms_edit', ['post' => $post]);
    }
}
