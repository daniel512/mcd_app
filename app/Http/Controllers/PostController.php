<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Post;
Use Exception;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::get();

        return view('posts')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
  
            $data = $request->all();
            $title = $data['postTitle'];
            $slug =  Str::slug(Str::lower($title));
            $json = json_encode($data['json']);

        try {
            Post::create([
                'user_id' => 1,
                'title' => $title,
                'slug' => $slug,
                'views' => 10,
                'image' => 'https://www.mieszkanieczydom.pl/public/images/bg.jpeg',
                'body' => $json,
                'published' => true
            ]);
        } catch (Exception $e) {
            dd($e);
    
            return false;
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function test()
    {
        $post = Post::find(28);
        dd($post->body);
        $category = Category::find($post->category_id);

        return view('test' ,['post'=> $post, 'category' => $category->name]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {

        $post = Post::where('slug', $slug)->first();
        $category = Category::find($post->category_id);
      //return view('post' ,['post'=> $post]);
        return view('post' ,['post'=> $post, 'category' => $category->name]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
