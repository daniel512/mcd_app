<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cms', 'CmsController@new');

Route::get('/cms_posts', 'CmsController@posts');

Route::get('/posts', 'PostController@index')->name('posts');

Route::get('/post/{slug}', 'PostController@show');

Route::post('/posts/create', 'PostController@create');

Route::resource('css', 'http://todolist.stg/css/app.css');

Route::get('/image', 'StorageController@test');
