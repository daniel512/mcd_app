<?php

use Illuminate\Database\Seeder;
use App\MyUser;

class MyUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $faker = \Faker\Factory::create();

        for($i = 0; $i < 5; $i++)
        {
            MyUser::create([
                'username' => $faker->name,
                'email' => $faker->email,
                'type' => 'regular',
                'password' => 'Test12345'
            ]);
        }
    }
}
