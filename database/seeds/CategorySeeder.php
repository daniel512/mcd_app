<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
 
    public function run()
    {
        Category::truncate();

        $categories = array('Inwestycje', 'Mieszkania', 'Porady');
        
        for($i = 0; $i < 3; $i++)
        {
            Category::create([
                'name' => $categories[$i]
            ]);
        }
    
    }
}
