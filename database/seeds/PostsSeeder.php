<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Post;

class PostsSeeder extends Seeder
{
  
    public function run()
    {
        Post::truncate();

        $faker = \Faker\Factory::create();

        $body = json_encode(array( 'elements' => 
        array(
            array('tag' => 'h2', 'content' => 'Flipping nieruchomości'),
            array('tag' => 'p', 'content' => 'Kupić tanio, sprzedać drogo – ten cel przyświeca wszystkim osobom zajmującym się handlem, w tym handlem nieruchomościami.'),
            array('tag' => 'h2', 'content' => 'Flipper – łowca okazji'),
            array('tag' => 'img', 'content' => 'https://www.mieszkanieczydom.pl/public/images/bg.jpeg'),
            array('tag' => 'p', 'content' => 'Flipping nieruchomości jest niczym innym, jak umiejętnością dostrzeżenia potencjału w nieruchomości, w której na pierwszy rzut oka jej nie widać.'),
        )));
            
        

        for($i = 0; $i < 15; $i++)
        {
            $sentence = $faker->sentence;
            Post::create([
                'user_id' => 1,
                'title' => $sentence,
                'slug' => Str::lower(Str::slug($sentence)),
                'views' => 10,
                'image' => 'https://www.mieszkanieczydom.pl/public/images/bg.jpeg',
                'body' => $body,
                'published' => true
            ]);
        }
    }
}
